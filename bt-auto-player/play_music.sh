#! /bin/sh

pulseaudio --start

pactl load-module module-bluetooth-discover

bluetoothctl << EOF
connect 11:58:02:A4:02:40 
EOF

pacmd set-default-sink bluez_sink.11_58_02_A4_02_40.a2dp_sink 
pacmd set-card-profile bluez_card.11_58_02_A4_02_40 a2dp_sink

cvlc -I http --http-port 43822 --http-user admin --http-password '1234' /home/pi/mp3_player/playlist.m3u


