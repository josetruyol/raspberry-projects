# MP3 Music Player with Bluetooth Speaker autoconnect

```console
$ chmod +x install.sh
$ ./install
```

Press `y` and `ENTER`. And then execute `sudo reboot`

```console
$ pactl load-module module-bluetooth-discover
$ bluetoothctl -a
[NEW] Controller YY:YY:YY:YY:YY:YY raspberrypi [default]
Agent registered
[bluetooth]# default-agent 
Default agent request successful
[bluetooth]# scan on
Discovery started
[CHG] Controller YY:YY:YY:YY:YY:YY Discovering: yes
[NEW] Device XX:XX:XX:XX:XX:XX btSpeaker
[bluetooth]# scan off
Discovery stopped
[CHG] Controller YY:YY:YY:YY:YY:YY Discovering: no
[bluetooth]# pair XX:XX:XX:XX:XX:XX
[bluetooth]# trust XX:XX:XX:XX:XX:XX
[bluetooth]# connect XX:XX:XX:XX:XX:XX

$ crontab -e
```

And add `@reboot /bin/sh /home/pi/play_music.sh`

Copy the `play_music.sh` to `/home/pi/` and edit it. Change `<BT_SPEAKER_ID>` and ` audio_file.mp3` to the desired values

